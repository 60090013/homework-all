package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	response, err := http.Get("http://api.airvisual.com/v2/countries?key=c722a555-a48d-4e5f-8c15-d2b65a5fad97")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)

	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))
		jsonFile, _ := os.Create("./data1.json")
		jsonFile.Write(data)

	}

	jsonData := map[string]string{"firstname": "Window", "lastname": "J"}
	jsonValue, _ := json.Marshal(jsonData)
	response, err = http.Post("http://httpbin.org/post", "application/json", bytes.NewBuffer(jsonValue))

	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)

	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))
		jsonFile, _ := os.Create("./data2.json")
		jsonFile.Write(data)
	}
}
